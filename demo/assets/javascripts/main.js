document.addEventListener("DOMContentLoaded", function() {
  Dennin.init();
  var dennin = Dennin.create().setDefaultKeyConfig().run();
  setTimeout(function(){
    Dennin.create().setKeyConfig({
      "goLeft": 74,
      "goRight": 76,
      "doJump": 73,
      "doFall": 75,
      "doAttack": 77
    }).run();
  }, 1000);
}, false);
