module Dennin {

  export interface Jump {
    power: number;
    limit: number;
    count: number;
    isJumping: boolean;
    isFloating: boolean;
  }
}
