module Dennin {

  export interface Rect {
    x: number;
    y: number;
    width: number;
    height: number;
    direction: boolean; // right: true, left: false
    isMoving: boolean;
  }
}
