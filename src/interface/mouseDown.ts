module Dennin {

  export interface MouseDown {
    prevEvent: MouseEvent;
    diffX: number;
    diffY: number;
  }
}
