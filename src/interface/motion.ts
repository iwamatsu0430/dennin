module Dennin {

  interface LeftRight {
    left: string;
    right: string;
  }

  export interface objectMotion {
    wait: LeftRight;
    move: LeftRight;
    stop: LeftRight;
    jump: LeftRight;
  }

  export interface playableMotion extends objectMotion {
  }

  export interface DenninMoion extends playableMotion {
    attack: LeftRight;
    fall: LeftRight;
  }
}
