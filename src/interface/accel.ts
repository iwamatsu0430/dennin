module Dennin {

  export interface Accel {
    x: number;
    y: number;
    unitX: number;
    unitY: number;
    minX: number;
    maxX: number;
    minY: number;
    maxY: number;
  }
}
