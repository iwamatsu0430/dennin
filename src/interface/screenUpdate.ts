module Dennin {

  export interface ScreenUpdate {
    move(): void;
    collision(): void;
    update(): void;
  }
}
