/// <reference path="environment/environment.ts" />

module Dennin {

  export var environment: clsEnvironment = new clsEnvironment();

  export function init(): void {
    globalSetting.isGameRunning = true;
    environment.init();
    console.log(clsDennin);
  }

  export function create(positionX: number = 0, positionY: number = 0): clsDennin {
    var dennin: clsDennin = new clsDennin(positionX, positionY);
    environment.addObject(dennin);
    return dennin;
  }

  export function bookmarklet(): void {
    init();
    create().setDefaultKeyConfig().run();
  }
}
