module Dennin {

  export class clsWindow {

    public rect: Rect;
    private resizeTimerHandler: any;

    constructor() {
      this.rect = {
        x: 0,
        y: 0,
        width: window.innerWidth,
        height: window.innerHeight,
        direction: true,
        isMoving: false
      }

      var that = this;
      window.addEventListener("resize", (e: UIEvent) => that.onResize(e, that));
    }

    private onResize(e: UIEvent, that: clsWindow): void {
      that.rect.width = window.innerWidth;
      that.rect.height = window.innerHeight;
      if (that.resizeTimerHandler != null) {
        clearTimeout(that.resizeTimerHandler);
      }
      that.resizeTimerHandler = setTimeout(() => {
        // resezed action
        that.resizeTimerHandler = null;
      }, Math.floor(1000 / 60 * 10));
    }
  }
}
