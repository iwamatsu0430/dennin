module Dennin {

  export class clsStage {
    public rect: Rect;
    public element: HTMLElement;

    constructor(element: HTMLElement) {
      this.element = element;
      this.rect = {
        x: this.element.offsetLeft,
        y: this.element.offsetTop,
        width: this.element.offsetWidth,
        height: this.element.offsetHeight,
        direction: true,
        isMoving: false
      };
    }
  }
}
