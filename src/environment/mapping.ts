module Dennin {

  export class clsMapping {

    private exceptionSelectors: string[] = [];
    public stages: clsStage[] = [];

    public addExceptionSelector(selector: string): void {
      if (this.exceptionSelectors.indexOf(selector) < 0) {
        this.exceptionSelectors.push(selector);
      }
    }

    public removeExceptionSelector(selector: string): void {
      var index: number = this.exceptionSelectors.indexOf(selector);
      if (index >= 0) {
        this.exceptionSelectors.splice(index, 1);
      }
    }

    public load(): void {
      var nodes = document.querySelectorAll("body *");
      this.stages = [];
      for (var i: number = 0; i < nodes.length; i++) {
        var element: HTMLElement = <HTMLElement><any>(nodes.item(i));
        if (!this.filter(element)) {
          continue;
        }
        this.stages.push(new clsStage(element));
      }
    }

    private filter(element: HTMLElement): boolean {
      var result: boolean = true;
      this.exceptionSelectors.forEach(selector => {
        if (typeof element.msMatchesSelector == "function") {
          result = !element.msMatchesSelector(selector) ? true : false;
        } else {
          var typeName = "function";
          eval("\
          if (typeof element.webkitMatchesSelector == typeName) {\
            result = !element.webkitMatchesSelector(selector) ? true : false;\
          } else {\
            result = false;\
          }"); // typescript compiler don't know webkitMatchesSelector
        }
      });
      return result;
    }
  }
}
