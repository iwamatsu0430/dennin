module Dennin {

    export class clsKeyEventHandler {

      protected handlersUp: {object: clsObject; handler: Function}[] = [];
      protected handlersDown: {object: clsObject; handler: Function}[] = [];
      private keyQueues: number[] = [];
      private prevKeyQueues: number[] = [];

      /**
      * add key up event
      */
      public addUp(handle: Function, calledObject: clsObject) {
        this.handlersUp.push({object: calledObject, handler: handle});
      }

      /**
      * add key down event
      */
      public addDown(handle: Function, calledObject: clsObject) {
        this.handlersDown.push({object: calledObject, handler: handle});
      }

      /**
      * remove mouse move event
      */
      public removeUp(calledObject: clsObject) {
        var index = -1;
        this.handlersUp.forEach((v, i) => {
          if (calledObject.id == v.object.id) {
            index = i;
          }
        });
        this.handlersUp.splice(index, 1);
      }

      /**
      * remove mouse move event
      */
      public removeDown(calledObject: clsObject) {
        var index = -1;
        this.handlersDown.forEach((v, i) => {
          if (calledObject.id == v.object.id) {
            index = i;
          }
        });
        this.handlersDown.splice(index, 1);
      }

      /**
      * remove key queue
      */
      public keyUp(keyCode: number): void {
        var index = this.keyQueues.indexOf(keyCode);
        if (index >= 0) {
          this.keyQueues.splice(index, 1);
        }
      }

      /**
      * add key queue
      */
      public keyDown(keyCode: number): void {
        if (this.keyQueues.indexOf(keyCode) < 0) {
          this.keyQueues.push(keyCode);
        }
      }

      /**
      * call all handlers
      * @param e event object
      */
      public call(): void {
        var upKeys: number[] = [];
        this.prevKeyQueues.forEach(keyCode => {
          if (this.keyQueues.indexOf(keyCode) < 0) {
            upKeys.push(keyCode);
          }
        });
        this.handlersUp.forEach(p => p.handler(this.keyQueues, p.object));
        this.handlersDown.forEach(p => p.handler(upKeys, p.object));
        this.prevKeyQueues = [];
        this.keyQueues.forEach(keyCode => this.prevKeyQueues.push(keyCode));
      }
    }
}
