module Dennin {

  export class clsGlobalEventHandler {

    protected handlers: {object: clsObject; handler: Function}[] = [];

    /**
    * add mouse move event
    */
    public add(handle: Function, calledObject: clsObject) {
      this.handlers.push({object: calledObject, handler: handle});
    }

    /**
    * remove mouse move event
    */
    public remove(calledObject: clsObject) {
      var index = -1;
      this.handlers.forEach((v, i) => {
        if (calledObject.id == v.object.id) {
          index = i;
        }
      });
      this.handlers.splice(index, 1);
    }

    /**
    * call all handlers
    * @param e event object
    */
    public call(e: Event): void {
      this.handlers.forEach(p => p.handler(e, p.object));
    }
  }
}
