/// <reference path="window.ts" />
/// <reference path="mapping.ts" />
/// <reference path="globalEventHandler.ts" />
/// <reference path="keyEventHandler.ts" />

module Dennin {

  var denninId: number = 0;

  /**
  * Dennin Environment
  * this class support overview
  * @author iwamatsu0430
  */
  export class clsEnvironment {

    public window: clsWindow = new clsWindow();
    public mouseMoveHandler: clsGlobalEventHandler = new clsGlobalEventHandler();
    public keyHandler: clsKeyEventHandler = new clsKeyEventHandler();

    private mapping: clsMapping = new clsMapping();
    private objectIds: number[] = [];
    private objects: clsObject[] = [];
    private selectCancelListener: EventListener = (e: Event) => e.preventDefault();

    /**
    * initialize Dennin environment
    */
    public init(): void {
      this.mapping.addExceptionSelector("script");
      this.map();
      this.setGlobalEvent();
      this.callLoop();
    }

    /**
    * map all html elements
    */
    public map(): void {
      this.mapping.load();
    }

    /**
    * get mapped stage
    * returns stage array
    */
    public getStages(): clsStage[] {
      return this.mapping.stages;
    }

    /**
    * Add new game object to environment
    * @param object game object
    */
    public addObject(object: clsObject) {
      this.objectIds.push(object.id);
      this.objects.push(object);
    }

    /**
    * Remove game object by ID.
    * @param id identify game object.
    */
    public removeObject(id: number) {
      var index: number = this.objectIds.indexOf(id);
      this.objectIds.splice(index, 1);
      this.objects.splice(index, 1);
    }

    /**
    * set global event
    */
    public setGlobalEvent() {
      window.addEventListener("keydown", (e: KeyboardEvent) => {
        this.keyHandler.keyDown(e.keyCode);
        if (e.keyCode >= 37 && e.keyCode <= 40) {
          e.preventDefault();
        }
      });
      window.addEventListener("keyup", (e: KeyboardEvent) => this.keyHandler.keyUp(e.keyCode));
      window.addEventListener("mousemove", e => this.mouseMoveHandler.call(e));
    }

    /**
    * Game Loop
    */
    private loop(that: clsEnvironment): void {
      if (typeof(that.objects) != "undefined") {
        this.keyHandler.call();
        that.objects.forEach(object => object.move());
        that.objects.forEach(object => object.collision());
        that.objects.forEach(object => object.update());
        that.map();
      }
      that.callLoop();
    }

    /**
    * call new game loop
    */
    public callLoop(): void {
      if (globalSetting.isGameRunning) {
        setTimeout(() => this.loop(this), 1000 / globalSetting.FPS);
      }
    }

    /**
    * document can't selected
    */
    public cancelSelectEvent(): void {
      document.addEventListener("selectstart", this.selectCancelListener);
    }

    /**
    * document can selected
    */
    public restoreSelectEvent(): void {
      document.removeEventListener("selectstart", this.selectCancelListener);
    }
  }
}
