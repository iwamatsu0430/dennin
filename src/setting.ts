module Dennin {

  export var globalSetting = {
    FPS: 30,
    isGameRunning: false,
    characterHTMLElementTagName: "div",

    className: {
      Dennin: "jsc-dennin-chracter"
    }
  };
}
