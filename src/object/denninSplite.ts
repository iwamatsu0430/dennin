/// <reference path="splite.ts" />

module Dennin {

  export class clsDenninSplite extends clsSplite {
    public motion: DenninMoion;

    constructor(rect: Rect) {
      super(rect);
      this.motion.attack = {
        left: "attack_left",
        right: "attack_right",
      };
      this.motion.fall = {
        left: "fall_left",
        right: "fall_right",
      };
    }
  }
}
