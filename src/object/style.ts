module Dennin {

  export class clsStyle {

    private styles: {[key: string]: string;} = {};

    public set(name: string, value: string): void {
      this.styles[name.toString()] = value.toString();
    }

    public remove(name: string): void {
      delete this.styles[name.toString()];
    }

    public toString(): string {
      var result: string[] = [];
      for (var key in this.styles) {
        result.push(key.toString() + ":" + this.styles[key]);
      }
      return result.join(";");
    }
  }
}
