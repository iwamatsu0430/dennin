/// <reference path="object.ts" />

module Dennin {

  export class clsPlayable extends clsObject {

    // ===================================================================================
    //                                                                        Action Event
    //                                                                        ============
    public abortJump(): void {
    }

    // ===================================================================================
    //                                                                         Key setting
    //                                                                         ===========
    public setDefaultKeyConfig(): clsPlayable {
      return this.setKeyConfig({
        "goLeft": 37,
        "goRight": 39,
        "doJump": 38,
        "doFall": 40,
        "doAttack": 65
      });
    }

    public setKeyConfig(keyMap: {[key: string]: number;}): clsPlayable {
      environment.keyHandler.addUp((keyQueues: number[], that: clsPlayable) => {
        var keyLeftIndex = keyQueues.indexOf(keyMap["goLeft"]);
        var keyRightIndex = keyQueues.indexOf(keyMap["goRight"]);
        if (keyLeftIndex >= 0 && keyRightIndex >= 0) {
          if (keyLeftIndex < keyRightIndex) {
            that.goLeft();
          } else {
            that.goRight();
          }
        } else {
          if (keyLeftIndex >= 0) {
            that.goLeft();
          }
          if (keyRightIndex >= 0) {
            that.goRight();
          }
        }

        keyQueues.forEach(keyCode => {
          switch(keyCode) {
            case keyMap["doJump"]:
              that.doJump();
              break;
            case keyMap["doFall"]:
              that.doFall();
              break;
            case keyMap["doAttack"]:
              that.doAttack();
              break;
          }
        });
      }, this);

      environment.keyHandler.addDown((upKeys: number[], that: clsPlayable) => {
        upKeys.forEach(keyCode => {
          switch(keyCode) {
            case keyMap["goLeft"]:
              that.goStop();
              break;
            case keyMap["goRight"]:
              that.goStop();
              break;
            case keyMap["doJump"]:
              that.abortJump();
              break;
          }
        });
      }, this);

      return this;
    }
  }
}
