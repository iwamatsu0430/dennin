module Dennin {

  export class clsObject implements ScreenUpdate{

    // ===================================================================================
    //                                                                          ID Factory
    //                                                                          ==========
    private static idCounter: number = 0;

    private static getIdByFactory(): number {
      return clsObject.idCounter++;
    }

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    public id: number;

    public splite: clsSplite;
    public rect: Rect;
    public accel: Accel;
    public jump: Jump;
    public element: HTMLElement;
    public mouseDown: MouseDown;

    public style: clsStyle = new clsStyle();
    public events: {[key: string]: Function;} = {
      "goLeft": () => {},
      "goRight": () => {},
      "goStop": () => {},
      "doAttack": () => {},
      "doFall": () => {},
      "doJump": () => {},
      "isJumping": () => {},
      "isFloating": () => {},
      "isLanding": () => {},
      "onRun": () => {},
      "onKill": () => {},
      "onOver": () => {},
      "onReborn": () => {},  // TODO
      "onCollisionWindow": () => {},
      "onCollisionStage": () => {},
      "onPause": () => {},  // TODO
      "onResume": () => {},  // TODO
      "onClick": () => {},
      "onMousedown": () => {},
      "onMousemove": () => {},
      "onMouseup": () => {},
    };

    public isDragging: boolean = false;
    public collisionStages: clsStage[] = [];

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    constructor(positionX: number = 0, positionY: number = 0) {
      this.id = clsObject.getIdByFactory();
      this.rect = {
        x: positionX,
        y: positionY,
        width: 0,
        height: 0,
        direction: true,
        isMoving: false
      };
      this.accel = {
        x: 0,
        y: 0,
        minX: 0,
        maxX: 0,
        minY: 0,
        maxY: 0,
        unitX: 0,
        unitY: 0
      };
      this.jump = {
        power: 0,
        limit: 0,
        count: 0,
        isJumping: false,
        isFloating: false
      };
      this.element = document.createElement(globalSetting.characterHTMLElementTagName);
      this.splite = this.setSplite(this.splite);
    }

    public setSplite(splite: clsSplite): clsSplite {
      return new clsSplite(this.rect);
    }

    // ===================================================================================
    //                                                                  Outside controller
    //                                                                  ==================
    public run(): clsObject {
      this.element.setAttribute("data-id", String(this.id));
      document.body.appendChild(this.element);
      var that = this;
      this.element.addEventListener("click", (e: MouseEvent) => that.onClick(e, that));
      this.element.addEventListener("mousedown", (e: MouseEvent) => that.onMousedown(e, that));
      environment.mouseMoveHandler.add((e: MouseEvent) => that.onMousemove(e, that), that);
      this.element.addEventListener("mouseup", (e: MouseEvent) => that.onMouseup(e, that));
      this.update();
      this.events["onRun"]();
      return this;
    }

    public kill(): clsObject {
      this.events["onKill"]();
      this.element.removeEventListener("click", null);
      this.element.removeEventListener("mousedown", null);
      environment.mouseMoveHandler.remove(this);
      this.element.removeEventListener("mouseup", null);
      document.body.removeChild(this.element);
      return this;
    }

    // ===================================================================================
    //                                                                       Screen Update
    //                                                                       =============
    public move(): void {}

    public collision(): void {
      this.collisionWindow();
      this.collisionStage();
    }

    protected collisionWindow(): void {
      var isCollision = false;
      if (this.rect.x < 0) {
        this.rect.x = 0;
        this.accel.x = 0;
        isCollision = true;
      }
      if (this.rect.x + this.rect.width > environment.window.rect.width) {
        this.rect.x = environment.window.rect.width - this.rect.width;
        this.accel.x = 0;
        isCollision = true;
      }
      if (isCollision) {
        this.onCollisionWindow();
      }
    }

    protected collisionStage(): void {
      this.jump.isFloating = true;
      this.collisionStages = [];

      var that = this;
      environment.getStages().forEach(stage => {
        // check collision
        if (that.rect.x > stage.rect.x + stage.rect.width || that.rect.x + that.rect.width < stage.rect.x) return;
        if (that.rect.y + that.rect.height > stage.rect.y && that.rect.y < stage.rect.y + stage.rect.height)
        {
          that.collisionStages.push(stage);
        }

        // check through
        if ((that.rect.y + that.rect.height) < stage.rect.y || (that.rect.y + that.rect.height) > (stage.rect.y + that.accel.y)) return;
        if (that.accel.y < 0) return;

        // set position
        that.rect.y = stage.rect.y - that.rect.height;

        // reset
        that.accel.y = 0;
        that.jump.isFloating = false;
      });

      if (this.jump.isFloating) {
        this.isFloating();
      } else {
        this.isLanding();
      }
      if (this.jump.isJumping) {
        this.isJumping();
      }
      if (this.collisionStages.length > 0) {
        this.onCollisionStage();
      }
    }

    public update(): void {
      var spliteKey = this.updateSpliteKey(this.splite.motion);
      this.splite.update(spliteKey);
      this.style.set("top", this.rect.y + "px");
      this.style.set("left", this.rect.x + "px");
      this.style.set("background-position", this.splite.position());
      this.element.setAttribute("style", this.style.toString());
    }

    protected updateSpliteKey(motion: objectMotion): string {
      if (this.isDragging) {
        if (this.rect.isMoving) {
          // return dragging move
        }
        // return dragging
      }
      if (this.jump.isFloating) {
        if (this.jump.isJumping) {
          // return jump
        }
        return this.rect.direction ? motion.jump.right : motion.jump.left;
      } else {
        if (this.rect.isMoving) {
          return this.rect.direction ? motion.move.right : motion.move.left;
        } else {
          return this.rect.direction ? motion.wait.right : motion.wait.left;
        }
      }
      return this.rect.direction ? motion.wait.right : motion.wait.left;
    }

    // ===================================================================================
    //                                                                      Event Register
    //                                                                      ==============
    public on(handlerName: String, handler: Function): clsObject {
      this.events[handlerName.toString()] = handler;
      return this;
    }

    public off(handlerName: String): clsObject {
      this.events[handlerName.toString()] = () => {};
      return this;
    }

    // ===================================================================================
    //                                                                        Action Event
    //                                                                        ============
    public goLeft(): void {
      this.events["goLeft"]();
      this.rect.direction = false;
      this.rect.isMoving = true;
    }

    public goRight(): void {
      this.events["goRight"]();
      this.rect.direction = true;
      this.rect.isMoving = true;
    }

    public goStop(): void {
      this.events["goStop"]();
      this.rect.isMoving = false;
    }

    public doJump(): void {
      this.events["doJump"]();
      this.jump.isJumping = true;
    }

    public doFall(): void {
      this.events["doFall"]();
    }

    public doAttack(): void {
      this.events["doAttack"]();
    }

    // ===================================================================================
    //                                                                     Condition Event
    //                                                                     ===============
    public isLanding(): void {
      this.events["isLanding"]();
    }

    public isFloating(): void {
      this.events["isFloating"]();
    }

    public isJumping(): void {
      this.events["isJumping"]();
    }

    public onCollisionStage(): void {
      this.events["onCollisionStage"]();
    }

    public onCollisionWindow(): void {
      this.events["onCollisionWindow"]();
    }

    public onOver(): void {
      this.events["onOver"]();
    }

    // ===================================================================================
    //                                                                         Mouse Event
    //                                                                         ===========
    public onClick(e: MouseEvent, that: clsObject): void {
      that.events["onClick"]();
    }

    public onMousedown(e: MouseEvent, that: clsObject): void {
      that.events["onMousedown"]();
      that.mouseDown = {
        prevEvent: e,
        diffX: e.x - that.rect.x,
        diffY: e.y - that.rect.y
      }
      environment.cancelSelectEvent();
    }

    public onMousemove(e: MouseEvent, that: clsObject): void {
      that.events["onMousemove"]();
    }

    public onMouseup(e: MouseEvent, that: clsObject): void {
      that.events["onMouseup"]();
      that.mouseDown = null;
      that.isDragging = false;
      environment.restoreSelectEvent();
    }

    // ===================================================================================
    //                                                                              Helper
    //                                                                              ======
    public drag(e: MouseEvent, that: clsObject): void {
      if (that.mouseDown != null) {
        that.isDragging = true;
        that.rect.x = e.x - that.mouseDown.diffX;
        that.rect.y = e.y - that.mouseDown.diffY;
      }
    }

    public suicide(ttl: number = 0): void {

    }
  }
}
