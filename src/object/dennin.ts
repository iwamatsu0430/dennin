/// <reference path="playable.ts" />

module Dennin {

  export class clsDennin extends clsPlayable {

    // ===================================================================================
    //                                                                           Attribute
    //                                                                           =========
    public windowCollision: boolean = true;
    public jumpStatus: boolean = false;

    // ===================================================================================
    //                                                                         Constructor
    //                                                                         ===========
    constructor(positionX: number = 0, positionY: number = 0) {
      super(positionX, positionY);

      this.rect.width = 48;
      this.rect.height = 48;

      this.accel.maxX = 12;
      this.accel.maxY = 20;
      this.accel.unitX = 2;
      this.accel.unitY = 1;

      this.jump.power = 15;
      this.jump.limit = 3;

      this.element.className = globalSetting.className.Dennin;

      this.style.set("z-index", "10000");
      this.style.set("position", "absolute");
      this.style.set("width", this.rect.width + "px");
      this.style.set("height", this.rect.height + "px");
      this.style.set("background-image", "url(" + this.splite.source + ")");
      this.style.set("cursor", "move");
    }

    public setSplite(splite: clsDenninSplite): clsDenninSplite {
      splite = new clsDenninSplite(this.rect);
      splite.source = resource.images.object.dennin;
      splite.add(splite.motion.wait.left,  {offsetX: 0, offsetY: 0, weight: 5});
      splite.add(splite.motion.wait.left,  {offsetX: 1, offsetY: 0, weight: 5});
      splite.add(splite.motion.wait.left,  {offsetX: 2, offsetY: 0, weight: 5});
      splite.add(splite.motion.wait.left,  {offsetX: 3, offsetY: 0, weight: 5});
      splite.add(splite.motion.wait.right, {offsetX: 0, offsetY: 1, weight: 5});
      splite.add(splite.motion.wait.right, {offsetX: 1, offsetY: 1, weight: 5});
      splite.add(splite.motion.wait.right, {offsetX: 2, offsetY: 1, weight: 5});
      splite.add(splite.motion.wait.right, {offsetX: 3, offsetY: 1, weight: 5});
      splite.add(splite.motion.move.left,  {offsetX: 0, offsetY: 2, weight: 2});
      splite.add(splite.motion.move.left,  {offsetX: 1, offsetY: 2, weight: 2});
      splite.add(splite.motion.move.left,  {offsetX: 2, offsetY: 2, weight: 2});
      splite.add(splite.motion.move.left,  {offsetX: 3, offsetY: 2, weight: 2});
      splite.add(splite.motion.move.right, {offsetX: 0, offsetY: 3, weight: 2});
      splite.add(splite.motion.move.right, {offsetX: 1, offsetY: 3, weight: 2});
      splite.add(splite.motion.move.right, {offsetX: 2, offsetY: 3, weight: 2});
      splite.add(splite.motion.move.right, {offsetX: 3, offsetY: 3, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 0, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 1, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 2, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 3, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 4, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 5, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 6, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.left,  {offsetX: 7, offsetY: 4, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 0, offsetY: 5, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 1, offsetY: 5, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 2, offsetY: 5, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 3, offsetY: 5, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 4, offsetY: 5, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 5, offsetY: 5, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 6, offsetY: 5, weight: 2});
      splite.add(splite.motion.jump.right, {offsetX: 7, offsetY: 5, weight: 2});
      return splite;
    }

    // ===================================================================================
    //                                                                       Screen Update
    //                                                                       =============
    public move(): void {
      if (this.isDragging) {
        this.accel.x = 0;
        this.accel.y = 0;
        return;
      }

      // accel Y
      if(this.jump.isFloating || this.jump.isJumping) {
        if (this.accel.y < this.accel.maxY) {
          this.accel.y += this.accel.unitY;
        } else {
          this.accel.y = this.accel.maxY;
        }
        this.rect.y += this.accel.y;
      }

      // accel down X
      if (this.accel.x != 0 && !this.rect.isMoving && !this.jump.isFloating) {
        if (this.rect.direction) {
          this.accel.x -= this.accel.unitX / 2;
        } else {
          this.accel.x += this.accel.unitX / 2;
        }
        if (Math.abs(this.accel.x) < 1) {
          this.accel.x = 0;
        }
      }

      this.rect.x += this.accel.x;
    }

    public collision(): void {
      if (this.isDragging) {
        return;
      }
      super.collision();
    }

    // ===================================================================================
    //                                                                        Action Event
    //                                                                        ============
    public goLeft(): void {
      super.goLeft();
      var value = this.accel.unitX * ((this.accel.x == 0) ? 3 : 1 );
      this.moveX(value * -1);
    }

    public goRight(): void {
      super.goRight();
      var value = this.accel.unitX * ((this.accel.x == 0) ? 3 : 1 );
      this.moveX(value * 1);
    }

    public moveX(value: number): void {
      // quick turn
      if ((this.accel.x > 0 && !this.rect.direction) || (this.accel.x <= 0 && this.rect.direction)) {
        this.accel.x *= -0.8;
      }

      if ((!this.jump.isFloating || this.accel.x == 0) && Math.abs(this.accel.x) <= this.accel.maxX) {
        this.accel.x += value;
      }

      this.rect.direction = (value >= 0) ? true : false;
    }

    public goStop(): void {
      super.goStop();
    }

    public doJump(): void {
      if (this.jump.count >= this.jump.limit) {
        return;
      }
      if (this.jumpStatus) {
        return;
      }
      super.doJump();
      this.accel.y = -1 * this.jump.power;
      this.jumpStatus = true;
    }

    public abortJump(): void {
      this.jump.count++;
      this.jumpStatus = false;
    }

    public doFall(): void {
      if (this.jump.isJumping || this.jump.isFloating) {
        return;
      }
      super.doFall();
      this.rect.y += this.accel.unitY;
    }

    public doAttack(): void {
      super.doAttack();
    }

    // ===================================================================================
    //                                                                     Condition Event
    //                                                                     ===============
    public isLanding(): void {
      super.isLanding();
      this.jump.isJumping = false;
      this.jump.count = 0;
    }

    public isJumping(): void {
      super.isJumping();
    }

    public onOver(): void {
      super.onOver();
    }

    // ===================================================================================
    //                                                                         Mouse Event
    //                                                                         ===========
    public onMousemove(e: MouseEvent, that: clsObject): void {
      super.onMousemove(e, that);
      that.drag(e, that);
    }
  }
}
