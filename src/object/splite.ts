module Dennin {

  export class clsSplite {

    public source: string = "";
    private animations: {[key: string]: {offsetX: number; offsetY: number; weight: number}[]} = {};

    public motion: objectMotion;

    private rect: Rect;
    private width: number;
    private height: number;
    private currentKey: string = "";
    private keyCounter: number = 0;
    private weightCounter: number = 0;

    constructor(rect: Rect) {
      this.rect = rect;
      this.motion = {
        wait: {
          left: "wait_left",
          right: "wait_right",
        },
        move: {
          left: "move_left",
          right: "move_right",
        },
        stop: {
          left: "stop_left",
          right: "stop_right",
        },
        jump: {
          left: "jump_left",
          right: "jump_right",
        }
      };
    }

    public add(key: string, pattern: {offsetX: number; offsetY: number; weight: number}): void {
      if (this.animations[key.toString()] == null) {
        this.animations[key.toString()] = [];
      }
      this.animations[key.toString()].push(pattern);
    }

    public update(key: string): void {
      if (this.currentKey == key) {
        return;
      }
      this.currentKey = key;
      this.keyCounter = 0;
      this.weightCounter = 0;
    }

    public position(): string {
      var frame = this.animations[this.currentKey];
      var x: number = -1 * frame[this.keyCounter].offsetX * this.rect.width;
      var y: number = -1 * frame[this.keyCounter].offsetY * this.rect.height;
      var output: string = x + "px " + y + "px";

      this.weightCounter++;
      if (this.weightCounter >= frame[this.keyCounter].weight) {
        this.weightCounter = 0;
        this.keyCounter++;
        if (this.keyCounter >= frame.length) {
          this.keyCounter = 0;
        }
      }
      return output;
    }
  }
}
