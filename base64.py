import os
import commands

# -----------------------------------------------------
#                                       Refer directory
#                                       ---------------
def referDir(dic, dirNames):
    if len(dirNames) == 0:
        return dic
    dirName = dirNames.pop(0)
    if (dirName in dic) is False:
        dic[dirName] = {}
    return referDir(dic[dirName], dirNames)

# -----------------------------------------------------
#                                         Create source
#                                         -------------
def createSource(dic):
    def printSpace(depth):
        return "  " * (depth + 1)
    def inner(src, dic, depth):
        for k in dic.keys():
            if isinstance(dic[k], dict):
                src += printSpace(depth) + k + ": {\n"
                src = inner(src, dic[k], depth + 1)
                src += printSpace(depth) + "},\n"
            else:
                fileName, ext = os.path.splitext(k)
                ext = ext.replace(".", "")
                fileType = ""
                if ext in {"png", "jpg", "jpeg", "gif"}:
                    fileType = "image"
                elif ext in {"wav", "mp3"}:
                    fileType ="audio"
                if dic[k] is not "":
                    src += printSpace(depth) + fileName + ": \"data:" + fileType + "/" + ext + ";base64," + dic[k] + "\",\n"
        return src
    return inner("", dic, 1)

# -----------------------------------------------------
#                                        Convert to dic
#                                        --------------
def convert2dic(target):
    structure = {}
    for walkObj in os.walk(target):
        if walkObj[0] == target:
            continue
        if len(walkObj[2]) == 0:
            continue

        targetDir = referDir(structure, walkObj[0].replace(target + "/", "").split("/"))
        for file in walkObj[2]:
            targetDir[file] = commands.getoutput("base64 " + walkObj[0] + os.sep + file)
    return structure

# -----------------------------------------------------
#                                     Output to ts file
#                                     -----------------
def outputTS(src):
    prefix = """module Dennin {

      export var resource = {
    """
    suffix = """  };
    }
    """
    f = open('./src/resource.ts', 'w')
    f.write(prefix + src + suffix)
    f.close()

# -----------------------------------------------------
#                                                  main
#                                                  ----
def main():
    structure = convert2dic("./assets")
    src = createSource(structure)

if __name__ == '__main__':
    main()
