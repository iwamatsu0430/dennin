#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import datetime
import signal
import time
import os
from stat import *
import commands

def watch(targets, command, exception):
    print("start watch file change.")
    print("if you want stop this program, Ctrl+C")
    timestamp = time.mktime(datetime.datetime.now().utctimetuple())
    while True:
        watchFiles = []
        for dir in targets:
            for info in os.walk(dir):
                for fileName in info[2]:
                    if 0 is fileName.find('.'):
                        continue
                    if fileName in exception:
                        continue
                    watchFiles.append(info[0] + os.sep + fileName)
        for file in watchFiles:
            file_timestamp = os.stat(file)[ST_MTIME]
            if timestamp < file_timestamp:
                timestamp = file_timestamp
                try:
                    print(commands.getoutput(command))
                    print("\nauto build at " + datetime.datetime.now().strftime('%Y/%m/%d %H:%M:%S') + "\n")
                except:
                    print("exception: " + file)
                break
        time.sleep(0.1)

def signalHandler(signal, frame):
    print("\nexit.")
    sys.exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signalHandler)
    watch(["./src", "./assets"], "sh compile.sh", ["resource.ts"])
