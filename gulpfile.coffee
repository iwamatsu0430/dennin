gulp        = require 'gulp'
sass        = require 'gulp-sass'
ts          = require 'gulp-typescript'
uglify      = require 'gulp-uglify'
shell       = require 'gulp-shell'
runSequence = require 'run-sequence'

gulp.task 'compile-sass', ->
  gulp.src 'demo/assets/sass/*.scss'
    .pipe sass()
    .pipe gulp.dest('demo/assets/stylesheets/')

gulp.task 'compile-base64',
  shell.task [
    'python base64.py'
  ]

gulp.task 'compile-typescript', ->
  gulp.src 'src/**/*.ts'
    .pipe ts
      noImplicitAny: true,
      removeComments: true,
      out: 'Dennin.js'
    .pipe gulp.dest 'bin/'

gulp.task 'duplicate',
  shell.task [
      'cp bin/Dennin.js bin/Dennin.min.js'
    ]

gulp.task 'uglify', ->
  gulp.src 'bin/Dennin.min.js'
    .pipe uglify()
    .pipe gulp.dest 'bin/'

gulp.task 'set-demo',
  shell.task [
    'cp bin/Dennin.js demo/assets/javascripts/',
    'cp bin/Dennin.min.js demo/assets/javascripts/'
  ]

gulp.task 'bookmarklet',
  shell.task [
    'prefix="javascript:(function(){"; src=`cat bin/Dennin.min.js`; suffix="Dennin.bookmarklet();})();"; echo $prefix$src$suffix > bin/Dennin.bookmarklet.js'
  ]

gulp.task 'build-ts', ->
  runSequence(
    ['compile-base64'],
    ['compile-typescript'],
    ['duplicate'],
    ['uglify'],
    ['set-demo'],
    ['bookmarklet']
  )

gulp.task 'default', ['compile-sass', 'build-ts']
