#!/bin/bash

command_exists() {
  command -v "$1" > /dev/null;
}

cd /vagrant

if ! command_exists node ; then
  sudo aptituude update
  yes y | sudo aptitude install nodejs
  sudo update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10
fi

if ! command_exists npm ; then
  sudo aptituude update
  yes y | sudo aptitude install npm
fi

if ! command_exists gulp ; then
  sudo npm install gulp -g
fi

python watch.py &
